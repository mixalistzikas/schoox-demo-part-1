# Schoox Demo (Part 1)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Further help

To get more help contact with <b>Michalis Tzikas</b> (mixalistzikas@gmail.com).
