export class Course {
    id?: number;
    title: string;
    price: number;
    edit?: boolean;

    constructor(id: number, title: string, price: number){
        this.id = id;
        this.title = title;
        this.price = price;
    }
}
