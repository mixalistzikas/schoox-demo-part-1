import {Component} from '@angular/core';
import {Course} from './course.model';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'schoox';
    initCourseNew = false;
    showAlertNewCourse = false;
    temp: any = {};
    newCourse: Course = {title: null, price: null};

    courses: Course[] = [
        {
            id: 1,
            title: 'How to solve a puzzle',
            price: 0
        },
        {
            id: 2,
            title: 'Angular for dummies',
            price: 10
        },
        {
            id: 3,
            title: 'The best of the best courses',
            price: 300
        }
    ];

    edit(course) {
        course.edit = true;
        this.temp[course.id] = {...course};
    }

    cancelEdit(course){
        course.edit = false;
        course.title = this.temp[course.id].title;
        course.price = this.temp[course.id].price;
    }

    saveEdit(course) {
        course.edit = false;
        this.temp[course.id] = null;
        // http request
    }

    saveNewCourse() {
        let maxId = Math.max(...this.courses.map(x => x.id));
        this.courses.push({...this.newCourse, id: ++maxId});
        this.newCourse = {title: null, price: null};
        this.initCourseNew = false;

        this.showAlertNewCourse = true;
        setTimeout(() => {
            this.showAlertNewCourse = false;
        }, 3000);
    }

    delete(course, index) {
        const answer = confirm('Are you sure?');
        if (!answer){
            return false;
        }
        this.courses.splice(index, 1);
    }

    getFontWeight(course) {
        console.log(`getFontWeight: ${course.id}`);
        return course.price > 0 ? null : 'bold';
    }
}
